# Release History

## v0.3.2

* Adds the `--format` CLI option

## v0.3.1

* Fixes compile-time bug caused by unset EDITOR environment variable

## v0.3.0

* Implements handlebars templates and spins off `libstubble` crate

## v0.2.3

* Adds ability to omit title by passing '-' instead

## v0.2.2

* Adds '--editor' flag

## v0.2.1

* Fixes 'Fall back on user name' for the `--author` option
* Allows multiple values for the `--tag` option

## v0.2.0

Adds extended command-line arguments.

## v0.1.1

Fixes a manifest typo and updates installation instructions in `README.md`

## v0.1.0

Initial release