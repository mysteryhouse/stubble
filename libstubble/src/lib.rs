//! `libstubble` provides APIs for quickly generating document stubs from common
//! properties.
//!
//! It was built to support [stubble](https://crates.io/crates/stubble), but
//! it's published separately for any other projects that may find it useful.
pub mod stub;
pub mod template;
