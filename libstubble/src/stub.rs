//! The `stub` module provides APIs and resources related to populating and
//! rendering content Stubs.
//!
//! The `*_TEMPLATE` `&str` constants are loaded from template files as compile time so
//! that built-in stub types can be used without installing any extra assets.
use crate::template;
use anyhow::Result;
use serde::{Deserialize, Serialize};

/// Describes built-in stub types.
pub enum Types {
    Markdown,
    Org,
}

impl Types {
    /// Returns the template for a given Stub type as a `&str`.
    pub fn template(&self) -> &str {
        match *self {
            Types::Markdown => template::MARKDOWN_TEMPLATE,
            Types::Org => template::ORG_TEMPLATE,
        }
    }
}

/// Properties that can be passed to `Stub` structs for rendering.
#[derive(Serialize, Deserialize)]
pub struct Props {
    /// The document title
    #[serde(rename = "title")]
    pub title: String,
    /// The document description / summary / abstract
    #[serde(rename = "description")]
    pub description: String,
    /// The document author's name
    #[serde(rename = "author")]
    pub author: String,
    /// The document publication date, in `YYYY-MM-DD` format
    #[serde(rename = "date")]
    pub date: String,
    /// A UUID string
    #[serde(rename = "uuid")]
    pub uuid: String,
    /// Tags that should be applied to the document
    #[serde(rename = "tags")]
    pub tags: Vec<String>,
}

/// `Stub` structs represent common properties that authors often need to
/// populate every time they begin a new document.
#[derive(Serialize, Deserialize)]
pub struct Stub {
    /// Contains the `Stub`'s properties.
    #[serde(flatten)]
    pub props: Props,

    /// Contains the current version of stubble.
    ///
    /// This will be automatically populated if you use the `Stub::new`` method
    /// to create a new `Stub`.
    #[serde(rename = "stubble_version")]
    pub stubble_version: String,
}

pub trait Render {
    /// Given a `String` containing a Handlebars template, render the template
    /// using the stub's properties and return a `String` `Result`.
    fn render(&mut self, template: String) -> Result<String>;
}

impl Stub {
    /// The new() method returns a `Stub` containing the given `Prop` instance.
    ///
    /// It automatically sets the `stubble_version` property to whatever version
    /// of `libstubble` is being used.
    pub fn new(props: Props) -> Stub {
        let stubble_version = std::env!("CARGO_PKG_VERSION").to_string();
        Stub {
            props,
            stubble_version,
        }
    }
}
