//! Argument handling for the `stubble` CLI.

use clap::Parser;

/// The `get_user_name` function returns the current user's full name, if available from the OS.
///
/// If a full name is not available, it returns the current user's username.
///
/// # Example
///
/// ```
/// use args::get_user_name;
///
/// let author_name: String = get_user_name();
///
/// println!("This is a post by {}", author_name);
/// ````
pub fn get_user_name() -> String {
    let author = if !whoami::realname().is_empty() {
        whoami::realname()
    } else {
        whoami::username()
    };
    author
}

#[derive(Parser)]
#[command(name = "Stubble")]
#[command(version = env!("CARGO_PKG_VERSION"))]
#[command(
    about = "Stubble is a command-line tool for quickly generating markdown files for use by static site generators. Given a title, it will generate a date-stamped markdown file with pre-populated YAML front-matter."
)]
/// clap::Parser to collect and parse arguments, arguments are stored as struct fields
pub struct Cli {
    #[arg(
        help = "Title of the post",
        long_help = "The title may be left empty by passing `-`. For example `stubble -`, would create a file like 1988-08-10.md"
    )]
    pub title: String,
    #[arg(default_value = ".", help = "Output destination")]
    pub dest: Option<String>,
    #[arg(short, long, help = "Optional slug to be used for the permalink")]
    pub slug: Option<String>,
    #[arg(num_args(0..),
        short,
        long,
        help = "Tag(s) to apply to the post. Separate multiple tags with spaces",
        long_help = r#"Tag(s) to apply to the post. Separate multiple tags with spaces.
        
Example usage with single tag:

    stubble post-title -t dogs

Example usage with multiple tags:

    stubble post-title -t dogs pets critters"#
    )]
    pub tag: Option<Vec<String>>,
    #[arg(short = 'D', long, help = "Override the post date: YYYY-MM-DD")]
    pub date: Option<String>,
    #[arg(short, long, help = "Post description")]
    pub description: Option<String>,
    #[arg(
        short,
        long,
        num_args(0..=1),
        help = "The name of the author",
        long_help = "The name of the author. Defaults to system provided 'real name' followed by username"
    )]
    pub author: Option<String>,
    #[arg(short, long, help = "The output format that should be used to render the stub.", default_value = "md", num_args(0..=1))]
    pub format: Option<String>, // TODO how best to constrain/display known types here without redundant data structures in libstubble::template?
    #[arg(short, long, help = "Override the default filename")]
    pub output: Option<String>,
    #[arg(
        short,
        long,
        num_args(0..=1),
        help = "Open in <EDITOR>",
        long_help = "If <EDITOR> is omitted, defaults to the value found in the env var $EDITOR\n
Example: run 'stubble \"Hello, World!\" -e' and stubble will open the stub in $EDITOR if set"
    )]
    pub editor: Option<Vec<String>>,
    #[arg(
        short,
        long,
        action,
        help = "Automatically answer 'Y' to all prompts (including existing file warnings)"
    )]
    pub yes: bool,
}
/// Collect and parse arguments passed on the cli into a clap::Parser
pub fn get_args() -> Cli {
    Cli::parse()
}
