use anyhow::{bail, Context, Result};
use chrono::{Local, NaiveDate};
use libstubble::stub::{Props, Stub};
use libstubble::template::Render;
use slug::slugify;
use std::fs::File;
use std::io::{BufWriter, Write};
use std::os::unix::process::CommandExt;
use std::{fs, process::Command};
use uuid::Uuid;
mod args;

use crate::args::get_args;

fn main() -> Result<()> {
    let m = get_args();
    // Get the optional output argument, which we will use as the output directory
    let mut dest_path = std::path::PathBuf::new();
    dest_path.push(m.dest.unwrap());

    let title = if m.title.eq_ignore_ascii_case("-") {
        String::from("")
    } else {
        m.title
    };
    // Create a slug and default to the title string if none was provided
    let slug = if m.slug.is_some() {
        slugify(m.slug.unwrap())
    } else {
        slugify(&title)
    };

    // Get user supplied date, or today's date
    let dt: NaiveDate = if m.date.is_some() {
        NaiveDate::parse_from_str(&m.date.unwrap(), "%Y-%m-%d").unwrap()
    } else {
        Local::now().date_naive()
    };
    let date_str = dt.format("%F");

    // TODO make sure author is only included if -a option was used
    // Decide which author to populate
    let author = if m.author.is_some() {
        m.author.unwrap()
    } else {
        args::get_user_name()
    };

    let format: String = m.format.unwrap_or_default();

    if m.output.is_some() {
        dest_path.push(m.output.unwrap());
    } else if title.eq_ignore_ascii_case("") {
        dest_path.push(format!("{}.{}", dt.format("%F"), format));
    } else {
        dest_path.push(format!("{}-{}.{}", dt.format("%F"), slug, format));
    }

    let uuid = Uuid::new_v4().to_string();

    let file_exists = fs::metadata(&dest_path).is_ok();

    if file_exists && !m.yes {
        let mut line = String::new();
        print!(
            "\nFile {} already exists. Do you want to overwrite it? (y/N) ",
            dest_path.display()
        );
        std::io::stdout().flush().unwrap();
        std::io::stdin().read_line(&mut line).unwrap();
        if line.trim().to_lowercase() != "y" {
            println!("\nCanceled.\n");
            return Ok(());
        }
    }
    let mut stub: Stub = Stub::new(Props {
        title,
        description: m.description.unwrap_or_default(),
        author: author,
        date: date_str.to_string(),
        uuid,
        tags: m.tag.unwrap_or_default(),
    });

    let content: String = stub.render_to(format)?;

    // This scope is required to ensure the file has been closed
    // before the editor tries to open it
    {
        let file_handle = File::create(&dest_path)
            .with_context(|| format!("\n\nCould not write to file {}", dest_path.display()))?;
        let mut writer = BufWriter::new(file_handle);

        let _ = write!(&mut writer, "{}", &content)?;
    }
    // Output success message with file path
    match file_exists {
        true => println!("\nReplaced file {} with a new stub.\n", dest_path.display()),
        false => println!("\nCreated new file {} !\n", dest_path.display()),
    }

    // only open the file in the editor if the flag was set and the string is not empty
    if m.editor.is_some() {
        let editor = m.editor.unwrap_or_default();
        let editor = if !editor.is_empty() {
            editor[0].to_string()
        } else {
            match std::env::var("EDITOR") {
                Ok(edt) => edt,
                Err(err) => {
                    bail!("No editor was specified and the $EDITOR environment variable is not set. ({:?})", err)
                }
            }
        };

        let exec_err = Command::new(&editor).args([dest_path]).exec();
        match exec_err.kind() {
            std::io::ErrorKind::NotFound => println!(
                "Error running editor \"{}\", is the executable name correct?",
                editor
            ),
            _ => panic!("Could not execute editor: {}, err {}", editor, exec_err),
        }
    }

    Ok(())
}
